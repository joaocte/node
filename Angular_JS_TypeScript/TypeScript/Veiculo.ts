export default class Veiculo{
    protected modelo: string
    protected velocidade: number = 0
    constructor() {
    }
    public Acelerar() : void
    {
        this.velocidade = this.velocidade + 10
    }
    public Parar() : void{
        this.velocidade = 0
    }

    public VelociadadeAtual() : number{
        return this.velocidade
    }
}
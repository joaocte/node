import { DaoInterface } from "./DaoInterface";
import  Concessionaria  from "./Concessionaria"
import { Dao } from "./Dao";

export class ConcessionariaDao implements Dao<Concessionaria> {
    nomeTabela: string = "tbConcessionaria"
    Insert(object: Concessionaria): boolean {
        console.log("Insert " + this.nomeTabela)
        return true
    }
    Update(object: Concessionaria): boolean {
        console.log("Update " + this.nomeTabela)
        return true;
    }
    Remove(id: number) : Concessionaria{
        console.log("Remove " + this.nomeTabela)
        return new Concessionaria('',[])
    }
    GetById(id: number) : Concessionaria {
        console.log("GetById " + this.nomeTabela)
        return new Concessionaria('',[])
    }
    GetAll(): Array<Concessionaria> {
        console.log("GetAll " + this.nomeTabela)
        return new Array<Concessionaria>()
    }


}
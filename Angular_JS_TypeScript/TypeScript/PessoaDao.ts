import { DaoInterface  } from './DaoInterface';
import Pessoa from './Pessoa'
import { Dao } from './Dao';

export class PessoaDao implements Dao<Pessoa>{
    nomeTabela: string = "tbPessoa";    
    
    Insert(object: Pessoa): boolean {
        console.log("Insert " + this.nomeTabela)
        return true
    }
    Update(object: Pessoa): boolean {
        console.log("Update " + this.nomeTabela)
        return true;
    }
    Remove(id: number) : Pessoa{
        console.log("Remove " + this.nomeTabela)
        return new Pessoa('','')
    }
    GetById(id: number) : Pessoa {
        console.log("GetById " + this.nomeTabela)
        return new Pessoa('','')
    }
    GetAll(): Array<Pessoa> {
        console.log("GetAll " + this.nomeTabela)
        return new Array<Pessoa>()
    }


}
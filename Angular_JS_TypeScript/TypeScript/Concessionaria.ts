import Carro from "./Carro";
import { ConcessionariaInterface } from "./ConcessionariaInterface";
export default class Concessionaria implements ConcessionariaInterface{
    GetHorarioDeFuncionamento(): string {
        return 'De Segunda à Sexta das 08:00 as 18:00, aos Sábados 08:00 à 12:00'
    }
    private endereco : string
    private listaDeCarros : Array<Carro>
    constructor(endereco: string, listaDeCarros: Array<Carro>){
        this.endereco = endereco
        this.listaDeCarros = listaDeCarros
    }

    public ObterEndereco() : string{
        return this.endereco
    }

    public ListarCarros() : Array<Carro>{
        return this.listaDeCarros
    }
}
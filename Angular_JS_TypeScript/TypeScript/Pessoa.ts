import Carro from "./Carro";

export default class  Pessoa{
    private nome: string
    private carroPreferido: string
    private carro: Carro
    constructor(nome: string, carroPreferido: string)
    {
        this.nome = nome
        this.carroPreferido = carroPreferido
    }

    public DizerNome(): string{
        return this.nome
    }
    public DizerCarroPreferido(): string{
        return this.carroPreferido
    }

    public ComprarCarro(carro: Carro) : void{
        this.carro = carro
    }
    public DizerCarroQueTem(): Carro{
        return this.carro
    }
}
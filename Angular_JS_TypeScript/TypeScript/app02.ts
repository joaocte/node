/*Realizar o import de qualquer tipo de coisa que estou exportando dentro da classe
import { Concessionaria  } from './Concessionaria'
*/
import Concessionaria from "./Concessionaria";
/*importar recursos default*/
import Pessoa from './Pessoa'
import  Carro from "./Carro";
// let carroA = new Carro("Modelo do Meu Carro", 5)
// console.log (carroA)


// let concessionaria  = new Concessionaria('Rua Utama, 130')
// console.log(concessionaria)


let Carros : Array<Carro> = [new Carro("Veloster", 5), new Carro("Corsa", 5)]

let concessionariaA: Concessionaria = new Concessionaria("Rua utama, 130", Carros)

 let pessoa  = new Pessoa('João Roberto','Veloster')
 
 concessionariaA.ListarCarros().map((carro:Carro)=> {
     if(carro['modelo'] == pessoa.DizerCarroPreferido())
     {
         pessoa.ComprarCarro(carro)
     }
 })
console.log(pessoa.DizerCarroQueTem())
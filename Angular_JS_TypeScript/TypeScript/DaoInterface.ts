export interface DaoInterface<T>{
    nomeTabela: string

    Insert(object: T) : boolean
    Update(object: T): boolean
    Remove(id : number): T
    GetById(id : number): T
    GetAll(): Array<T>

}
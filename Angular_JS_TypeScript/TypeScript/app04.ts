import { DaoInterface } from "./DaoInterface";
import { PessoaDao } from "./PessoaDao";
import { ConcessionariaDao } from "./ConcessionariaDao";
import Pessoa from "./Pessoa";
import Concessionaria from "./Concessionaria";

let daoPessoa : DaoInterface<Pessoa> = new PessoaDao
let daoConcessionaria : DaoInterface<Concessionaria> = new ConcessionariaDao


let pessoa = new Pessoa('','');

let concessionaria = new Concessionaria('',[])

daoPessoa.Insert(pessoa);
daoPessoa.Update(pessoa)

daoConcessionaria.Insert(concessionaria)
daoConcessionaria.GetAll()
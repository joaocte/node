"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Pessoa_1 = __importDefault(require("./Pessoa"));
var PessoaDao = /** @class */ (function () {
    function PessoaDao() {
        this.nomeTabela = "tbPessoa";
    }
    PessoaDao.prototype.Insert = function (object) {
        console.log("Insert " + this.nomeTabela);
        return true;
    };
    PessoaDao.prototype.Update = function (object) {
        console.log("Update " + this.nomeTabela);
        return true;
    };
    PessoaDao.prototype.Remove = function (id) {
        console.log("Remove " + this.nomeTabela);
        return new Pessoa_1.default('', '');
    };
    PessoaDao.prototype.GetById = function (id) {
        console.log("GetById " + this.nomeTabela);
        return new Pessoa_1.default('', '');
    };
    PessoaDao.prototype.GetAll = function () {
        console.log("GetAll " + this.nomeTabela);
        return new Array();
    };
    return PessoaDao;
}());
exports.PessoaDao = PessoaDao;

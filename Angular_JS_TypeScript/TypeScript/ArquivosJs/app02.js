"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/*Realizar o import de qualquer tipo de coisa que estou exportando dentro da classe
import { Concessionaria  } from './Concessionaria'
*/
var Concessionaria_1 = __importDefault(require("./Concessionaria"));
/*importar recursos default*/
var Pessoa_1 = __importDefault(require("./Pessoa"));
var Carro_1 = __importDefault(require("./Carro"));
// let carroA = new Carro("Modelo do Meu Carro", 5)
// console.log (carroA)
// let concessionaria  = new Concessionaria('Rua Utama, 130')
// console.log(concessionaria)
var Carros = [new Carro_1.default("Veloster", 5), new Carro_1.default("Corsa", 5)];
var concessionariaA = new Concessionaria_1.default("Rua utama, 130", Carros);
var pessoa = new Pessoa_1.default('João Roberto', 'Veloster');
concessionariaA.ListarCarros().map(function (carro) {
    if (carro['modelo'] == pessoa.DizerCarroPreferido()) {
        pessoa.ComprarCarro(carro);
    }
});
console.log(pessoa.DizerCarroQueTem());

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var PessoaDao_1 = require("./PessoaDao");
var ConcessionariaDao_1 = require("./ConcessionariaDao");
var Pessoa_1 = __importDefault(require("./Pessoa"));
var Concessionaria_1 = __importDefault(require("./Concessionaria"));
var daoPessoa = new PessoaDao_1.PessoaDao;
var daoConcessionaria = new ConcessionariaDao_1.ConcessionariaDao;
var pessoa = new Pessoa_1.default('', '');
var concessionaria = new Concessionaria_1.default('', []);
daoPessoa.Insert(pessoa);
daoPessoa.Update(pessoa);
daoConcessionaria.Insert(concessionaria);
daoConcessionaria.GetAll();

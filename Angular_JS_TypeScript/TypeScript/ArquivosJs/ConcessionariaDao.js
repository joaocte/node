"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Concessionaria_1 = __importDefault(require("./Concessionaria"));
var ConcessionariaDao = /** @class */ (function () {
    function ConcessionariaDao() {
        this.nomeTabela = "tbConcessionaria";
    }
    ConcessionariaDao.prototype.Insert = function (object) {
        console.log("Insert " + this.nomeTabela);
        return true;
    };
    ConcessionariaDao.prototype.Update = function (object) {
        console.log("Update " + this.nomeTabela);
        return true;
    };
    ConcessionariaDao.prototype.Remove = function (id) {
        console.log("Remove " + this.nomeTabela);
        return new Concessionaria_1.default('', []);
    };
    ConcessionariaDao.prototype.GetById = function (id) {
        console.log("GetById " + this.nomeTabela);
        return new Concessionaria_1.default('', []);
    };
    ConcessionariaDao.prototype.GetAll = function () {
        console.log("GetAll " + this.nomeTabela);
        return new Array();
    };
    return ConcessionariaDao;
}());
exports.ConcessionariaDao = ConcessionariaDao;

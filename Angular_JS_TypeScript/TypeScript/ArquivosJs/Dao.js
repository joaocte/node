"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Dao = /** @class */ (function () {
    function Dao() {
        this.nomeTabela = "tbConcessionaria";
    }
    Dao.prototype.Insert = function (object) {
        console.log("Insert " + this.nomeTabela);
        return true;
    };
    Dao.prototype.Update = function (object) {
        console.log("Update " + this.nomeTabela);
        return true;
    };
    Dao.prototype.Remove = function (id) {
        console.log("Remove " + this.nomeTabela);
        return Object();
    };
    Dao.prototype.GetById = function (id) {
        console.log("GetById " + this.nomeTabela);
        return Object();
    };
    Dao.prototype.GetAll = function () {
        console.log("GetAll " + this.nomeTabela);
        return [Object()];
    };
    return Dao;
}());
exports.Dao = Dao;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Concessionaria = /** @class */ (function () {
    function Concessionaria(endereco, listaDeCarros) {
        this.endereco = endereco;
        this.listaDeCarros = listaDeCarros;
    }
    Concessionaria.prototype.GetHorarioDeFuncionamento = function () {
        return 'De Segunda à Sexta das 08:00 as 18:00, aos Sábados 08:00 à 12:00';
    };
    Concessionaria.prototype.ObterEndereco = function () {
        return this.endereco;
    };
    Concessionaria.prototype.ListarCarros = function () {
        return this.listaDeCarros;
    };
    return Concessionaria;
}());
exports.default = Concessionaria;

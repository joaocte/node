import { Component } from "@angular/core";

@Component(
    {
        selector: 'app-topo',
        //como classe css
        //selector: '.app-topo',
        //como atributo de uma tag
        //selector: ['app-topo'],
        //Quando nos referimos a template estou me referindo a view ou ao html
        templateUrl: './topo.component.html',
        //styles: [' .exemplo {color: red} ']
        styleUrls: ['./topo.component.css']
        //na situação abaixo não posso quebrar linha.
        //template: '<p>Este é o topo</p>'
        //com `` eu posso quebrar a linha
        /*template: `<p>Este é o t
        opo</p>`*/

    })
export class TopoComponent{}
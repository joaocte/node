//não preciso nem do {} pois é uma instrução simples
var dobroDoValor = numero => numero * 2

//como tenho apenas um parametro não necessito do () para numero podendo ficar conforme abaixo
// var dobroDoValor = numero => {
//     return numero * 2
// }


//arrowFunction 
// var dobroDoValor = (numero) => {
//     return numero * 2
// }

//função no ES5, com a atualização para o ES6 houve algumas mudanças.
/*var dobroDoValor = function(numero){
    return numero * 2
}*/
console.log(dobroDoValor(7))
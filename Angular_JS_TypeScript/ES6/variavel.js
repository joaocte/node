var serie = 'Friends'

const variavelConstante = 'House of Cards'

//as variaveis do tipo const não pode sofre alteração / receber atribuição conforme abaixo
//variavelConstante = 'XPTO'
if(true)
{
    //se eu usar let a variável vai existir somente dentro do blobo conforme o código abaixo
    // para sofrer elevação será necessário o type var
    let serie2 = 'Game of Thrones'
    console.log(serie2)
}

//Elevação de Escopo
console.log(serie);

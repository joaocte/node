const express = require('express')

const rot = express.Router()

rot.use((req, res, next) =>
{
    const init = Date.now()
    next()
    console.log(`Tempo = ${Date.now() - init} ms.`)
})

rot.get('/produtos/:id', (req, res) => {
    res.json({
        id: req.params.id, name: 'Caneta'
    })
})

rot.get('/clientes/:id', (req, res) => {
    res.json({id: req.params.id, name: 'Joao'})
})

module.exports = rot
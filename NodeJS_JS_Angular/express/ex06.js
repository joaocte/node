/*
    quando uso require para obter uma instancia do objeto igual a seguir ' const express1 = require('express') ' ele retorna
    sempre uma mensma instancia do objeto
*/
const express1 = require('express')
const express2 = require('express')

console.log(express1 === express2)

/*
    nos exemplos abaixo a instancia para a ser diferente pois nao lembro o pq.
*/
const server1 = express1()
const server2 = express1()

console.log(server1 === server2)

const router1 = express1.Router()
const router2 = express1.Router()

console.log(router1 === router2)
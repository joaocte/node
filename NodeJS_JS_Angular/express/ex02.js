const express = require('express')
const server = express()

/*
    neste caso estou tendo chain of responsability....

    onde a primeira função do arquivo chama a proxima a ser executada

*/
server.get('/', function(req, res, next){
    console.log('Inicio..')
    next()
    console.log('Fim..')
})

server.get('/', function(req, res){
    console.log('Resposta..')
    res.send('<h1>Alá Express</h1>')
})

server.listen(3000,()=> console.log('Exe'))
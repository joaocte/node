const express = require('express')
const server = express()


/*
    Dessa forma eu consigo mapear todos os métodos desejados para uma mesma url
*/
server.route('/clientes')
.get((req,res)=> res.send("lista de Clientes"))
.post((req,res)=> res.send("Novo Cliente"))
.put((req,res)=>res.send("Alterando Cliente"))
.delete((req,res)=> res.send("Deletando Cliente"))

server.listen(3000,()=> console.log('Exe'))


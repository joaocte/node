const express = require('express')
const server = express()

/*
    neste caso estou tendo chain of responsability....
    a diferença do use para all é que neste caso minha url tem que começar com o que eu determinar na url
    neste caso api
*/
server.use('/api', function(req, res, next){
    console.log('Inicio..')
    next()
    console.log('Fim..')
})

server.use('/api', function(req, res){
    console.log('Resposta..')
    res.send('<h1>API!</h1>')
})

server.listen(3000,()=> console.log('Exe'))


/*
com as duas funções descritas abaixo qualquer url da minha aplicacao irá passar por estas funções pois determinei que 
qualquer endereço deverá passar por estas funçoes
server.use(function(req, res, next){
    console.log('Inicio..')
    next()
    console.log('Fim..')
})

server.use(function(req, res){
    console.log('Resposta..')
    res.send('<h1>API!</h1>')
})

server.listen(3000,()=> console.log('Exe'))
*/
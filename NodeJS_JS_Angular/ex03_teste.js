const s1 = require('./ex03_singleton')
const s2 = require('./ex03_singleton')

/*
    este caso retorna sempre a mesma instancia de um objeto
*/

s1.exibirProximo()
s2.exibirProximo()

s1.exibirProximo()
s2.exibirProximo()